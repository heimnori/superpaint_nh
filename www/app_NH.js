window.onerror = function(err){
    alert(err);
}

document.addEventListener('deviceready', function(){ 
    console.log('Device ready')


    $('#myCanvas').attr({
        width:300,
        height:300
    })

    var meinCanvas = $('#myCanvas')[0]

    var ctx = meinCanvas.getContext("2d");

    var x1, x2, y1, y2, penactive;
    

    $('canvas').ontouchstart = function (event){
        console.log('start ok')
        x1 = event.offsetX
        y1 = event.offsetY
        penactive = true;
        console.log('aktiv')

        ctx.moveTo(x1,y1)
        //ctx.lineTo(100,100)
        //ctx.stroke()      

    }//mousedown

    $('canvas').ontouchmove = function(event){
        console.log('move geht')
        x2 = event.offsetX
        y2 = event.offsetY
        //console.log(x2,y2)
        if(penactive==true){
            ctx.lineTo(x2,y2)
            ctx.stroke()
            x1=x2;
            y1=y2;
        }
        
    }//mousemove


    $('canvas').mouseup(function(){
        penactive = false;
        console.log('inaktiv')
    })//mouseup

    $('#cambtn').on('click', function(){

        navigator.camera.getPicture(

            function(image){
                var bild = new Image();
                bild.src = image;
                bild.onload = function(){
                    var ctx = $('#myCanvas').get(0).getContext('2d')
                    ctx.drawImage(bild,0,0)
                }
            },
            function(){
                alert('fehler')
            },
            {
                targetWidth: 200,
                targetHeight:200,
                correctOrientation: true,
            }

        ); //getPicture diese Funktion kommt vom Cordova Plugin(auf die kamera kann man nur mit Java zugreifen, Cordova 'übersetzt')


$('#savebtn').on('click', function(){
        window.canvas2ImagePlugin.saveImageDataToLibrary(
            function(){alert('daten gespeichert')},
            function(){alert('Fehler passiert')}
        );

})

    })
    


    




}) //diese Datei geht nur, wenn Phonegap gestartet wird